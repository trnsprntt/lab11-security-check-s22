# Lab11 -- Security check


## Introduction

Finally, this is the last lab in this semester. You will or you already have been creating applications that are rather sensitive to security so you want to check that your application is secure enough. And maybe you think that you are not competent enough for security testing or maybe the cost of failure is rather big in your case, but usually you want to delegate this work to the other institution to let them say if the system is secure enough. And in this lab, you would be that institution. ***Let's roll!***

## OWASP

[OWASP](https://owasp.org/) is Open Web Application Security Project. It is noncommercial organization that works to improve the security of software. It creates several products and tools, which we will use today and I hope you will at least take into account in future.

## OWASP Top 10

[OWASP Top 10](https://owasp.org/www-project-top-ten/) is a project that highlights 10 current biggest risks in software security. Highlighting that they make it visible. And forewarned is forearmed. As a software developer, you may at least consider that risks.

## OWASP Cheat Sheet Series

[OWASP CSS](https://cheatsheetseries.owasp.org/) is a project that accumulates the information about security splitted into very specific topics. And sometimes that might answer the question "How to make it secure?" according to specific topics (e.g. forgot password functionality).

## Google security check

Let's check how Google responds to the OWASP Cheatsheets of Forgot Password topic.

### Select the requirement from cheatsheets

E.g. how Google provides the forgot password feature and whether it "Return a consistent message for both existent and non-existent accounts."([link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)). To check this we should only try to reset password for existent and unexistent user and compare result.

Note: _In your work is important to make link to particlular requirement in cheatsheet._

### Create and execute test case

Up to this moment we do not know what would be during testing forgot password feature at google.com. Not only real result, but even steps. But we can expect some steps in this process and some expected result:
1. Beforehand we have to know some existent account. 
2. Open google.com page
3. Try to authenticate
4. Understand that we forgot the password
5. Try to reset password for some unexistent account
6. Remember the result
7. Go back and make the same for the prepared account
8. Remember the result 
9. Expect that results for 2 users would be indistinguishable, or the intruder might process [user enumeration atack](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=user%20enumeration%20attack)

Note: _You should not provide this high level steps, you might take it into account and go to next step to documenting test case execution_

Now lets try to execute that with documenting each step, to let yourself or someone another to reproduce it. During execution some unexpected issues might happen. It is ok, if the planned test case is not changd a lot just mention it into result section. Or if original plan changes a lot then return to it, review it, change if needed and continue.

| Test step  | Result |
|---|---|
| Open google.com  | Ok |
| Push sign in button  | Login page opened. If you have already logged in log out and repeate  |
| Push forgot email  |  There is no forgot password, but we can push forgot email. It still starts password recovery |
| Open new tab with sign in page  |  I tried to input unexistent account and understand that i do not know whether the account exists. So I would try to register with new account without submiting the form, so i would be sure about email that does not exists |
| Push create account button  |  Ok |
| Push create for yourself tab  |  Ok, create account form opened |
| Input your existent account email into email field  |  Ok, field highlight red |
| Try to change this email to make it unique  |  Ok, also you can input some name and page offer you new unique email. Remember that email |
| Return to forgot password page and input the unexistent email and push submit  |  Ok, form with first and last names opened |
| Input some first and last names and push submit |  Since you do not know the names of unexistent account it is ok to imagine somehting |
| See the result that there is no such an account  |  Ok, remember this result |
| Push retry button  |  Ok, we in the start of forgot password process |
| Input prepared existent email and push submit |  Ok, form with last and first name opened |
| Input first and last names and push submit  |  Ok, since it is prepared account you know its first and last names |
| See the page with confirmation  to send resetting email |  Ok. Remember this result |

Thats all, we tried 2 accounts and get 2 results, they are different so test case failed

### Analysis

Test case was failed, and according to different result intruders might process user enumeration attack and get database with existing users emails names and surnames, at least there is such an opportunity. Of course to brute force every email it require a lot of resources, but in this case it requires much more, because with email intruder need to pick up its name and surname. So even test case failed, that is not an security issue in given context.

Interesting thing that for my account, the email with resetting information would be sent on the same email(that i want to restore password)

Note: _Do not consider this result analysis as guide to action. Just see the test case result and provide your thoughts about_

## Homework

### Goal

To get familiar with OWASP CSS project by practicing limited security blackbox testing based on its materials. The default topics for testing is "Forgot password" and "Files Upload", but we are appeciate you to read another topics and select interesting one.

Note: _You are free to select topics or even combine topics, but remember that you should be able to test it using black box testing_

### Task

1. Pick a website that you can test using blackbox mode and put it into your solution and check that it is unique with previous submissions(web site and topic tested should be unique toghether, e.g. google.com might be tested on forgot password in one submission, and on file upload in another one). Like it was during [lab 9](https://gitlab.com/sqr-inno/lab10-exploratory-testing-and-ui-testing-s22#homework)(If it is hard for you to find such website just google "Forgot password" and you will see list of forgot password pages, just select one for yourself, or if you select another topic, then google it to find websites and pages with another features)
2. Find specific advice/requirement/standard or similar that you want to check whether your picked website follows it. By default look into ["Forgot password"](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html) and ["File upload"](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html) topics, because there are very straight forward checklists, with possible black box testing. But again, you are free(And I appreciate that) to select any topics (Be sure that you are able to ensure that during black box testing)
3. For entity from previous stage imagine the flow of test(you do not need to document it), its inputs, high level steps, and expected results.
4. Follow that flow with documenting each step and results
5. Comment the results of test cases. Is that everything ok, maybe it is a found vulnerability or weakness, or it might be intended weaking of the security protection in given context, what might be an alternative solution.(This is not a checklist that you must follow, just take it into account as line of thought)
6. Repeat from the first point(actually the website might be the same with different topic or entity from second point, and you do not need to mention web site each time) 4 times(the one from the lab is not counted). 

### Notes
1. Test cases might be executed fully manually, using helper tools, or fully automated, but in this cases I prefer to see the source code or know the used tool.
2. I want to see the linkage between entities from points 2,3,4,5 and with the referenced place from OWASP CSS(hope the structure would not change between your submission and my evaluation)
3. Some advices might be easy to test(e.g. password form should contain minimum password length validation), but some of them might require a lof of efforts(e.g. Ensure that the time taken for the user response message is uniform for existent user and unexistent). So 4 tests is just a recomendation it might be higher or lower. Finnaly I can reject submission if see too less effort put into the work. So to write me directly to prereview selected topics and proposed flows might be good idea(but not mandatory).


I am testing the file uploading functionality for the website https://www.freepdfconvert.com/

**1. Only allow authorized users to upload files**
| Step                                 | Status   |
| --------------------------------------------- | -------- |
|Go to https://www.freepdfconvert.com/ | OK |
| If you have already logged in. log out of the system | I was not signed in, so skipping this step |
| Click 'Choose file' button | OK. Ideally, it should not have appeared for a non-authorized user, but we'll proceed, probably it does not work|
| Choose file and upload | OK. The system suggests the extension to convert the file |
| Choose 'To Word' | OK |
| The file was successfully uploaded and converted | OK |

![](https://i.imgur.com/SRdA2zg.png)

#### Conclusion:
It turned out that after a certain number of uploads, the system stops this functionality for an hour for unregistered user. This, however, could be overcomed by VPN. Still, allowing unauthorized user to upload files violates security guidelines and would better be restricted. 
![](https://i.imgur.com/m6ZJ5fe.png)

**2. List allowed extensions. Only allow safe and critical extensions for business functionality**
| Step                                 | Status   |
| --------------------------------------------- | -------- |
|Go to https://www.freepdfconvert.com/ | OK |
| See the list of available extensions | OK. It says "Select the Word, Excel, PowerPoint, PDF or other file you wish to convert." So, the extensions are not explicitly mentioned|
| Click 'Choose file' button | OK |
| Upload a file with a rubbish extension | I am uploading .weirdext file |
| The system should not accept this file | OK |

![](https://i.imgur.com/BuzC26G.png)
![](https://i.imgur.com/xg11I9a.png)

#### Conclusion:
The extensiouns should be listed more explicitly. Although providing a wide functionality with numerous extensions, the system still check for allowed ones, which is good.

**3. Validate the file type, don't trust the Content-Type header as it can be spoofed**

I used the following information from the website to create CURL-requests

![](https://i.imgur.com/eoTMZ6C.png)

| Step                                 | Status   |
| --------------------------------------------- | -------- |
| Upload a pdf-file to website api using CURL (1). A file uploaded format - PDF, Content-Type header - PDF | OK - 200, as expected |
| Upload a .weirdext file using CURL (2). A file uploaded format - .weirdext, Content-Type header - .weirdext | OK - 415 (Unsupported Media Type) - as expected |
| **Upload a .weirdext file using CURL (3). A file uploaded format - .weirdext, Content-Type header - PDF** | **200 Status code, although the expected is 415** |




1. `curl -i -X POST https://api.freepdfconvert.com/upload -H "Content-Type: application/pdf" --data-binary "@/Users/trnsprntt/Documents/sqrs/lab10/test-file.pdf"`
  ![](https://i.imgur.com/14J1Yu9.png)

2. `curl -i -X POST https://api.freepdfconvert.com/upload \
  -H "Content-Type: application/weirdext" \
  --data-binary "@/Users/trnsprntt/Documents/sqrs/lab10/file.weirdext"`
  ![](https://i.imgur.com/XyZtHcg.png)

3. `curl -i -X POST https://api.freepdfconvert.com/upload \
  -H "Content-Type: application/pdf" \
  --data-binary "@/Users/trnsprntt/Documents/sqrs/lab10/file.weirdext"`
  ![](https://i.imgur.com/A9Bxoq8.png)

#### Conclusion:
This functionality is **comletely unsecure**, since it is able to upload any kind of file to the system misleading it by the request header.

**4. Change the filename to something generated by the application**
| Step                                 | Status   |
| --------------------------------------------- | -------- |
|Go to https://www.freepdfconvert.com/ | OK |
| Click 'Choose file' button | OK |
| Choose file with name '20199065-616204369.pdf' and upload | OK. The system suggests the extension to convert the file |
| Check if the name of the uploaded file is the same or it was regenerated with the system | OK, **the name did not change** |
| Choose 'To Word' convertion option and see what happens to the file name | OK, **the file name stayed the same** |

![](https://i.imgur.com/s2nhJyV.png)

![](https://i.imgur.com/Qenw2y0.png)

![](https://i.imgur.com/6GVDBfX.png)

#### Conclusion:
Although it might be more convenient for the user to have the same filename, it is more safe to rename files in the system. First of all, to lower the probability of brute-forcing filenames, and second, for OS-compatibility purposes.

**5. Set a filename length limit. Restrict the allowed characters if possible**
| Step                                 | Status   |
| --------------------------------------------- | -------- |
|Go to https://www.freepdfconvert.com/ | OK |
| Click 'Choose file' button | OK |
| Choose file with name 255-characters long (this is the maximum for Mac) and upload | OK. The system successfully uploads the file |
 
The file name also contained all the characters allowed on Mac OS, so the character estriction could not be tested on other symbols

#### Conclusion:
Although we could not for sure verify whether the filename limit is set, still 255 characters is unreasonably much.

**6. Set a file size limit**

The application promises to process unlimited file sized in paid version, but for now I am testing a free one
![](https://i.imgur.com/g9JmQFq.png)

| Step                                 | Status   |
| --------------------------------------------- | -------- |
| Go to https://www.freepdfconvert.com/ | OK |
| Click 'Choose file' button | OK |
| Choose file with 1.23 GB weigth | OK. The system successfully uploads the file |

#### Conclusion:
Although we could not for sure verify whether the filesize limit is set, still more than 1GB is unreasonably much for an unauthorized free user.

